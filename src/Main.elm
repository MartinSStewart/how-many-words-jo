module Main exposing (..)

import Browser
import Browser.Events
import Element exposing (Element)
import Element.Font as Font
import Html exposing (Html)
import Round
import Time



---- MODEL ----


type alias Model =
    { time : Int }


init : Int -> ( Model, Cmd Msg )
init millisSinceEpoch =
    ( { time = millisSinceEpoch }, Cmd.none )



---- UPDATE ----


type Msg
    = AnimationFrame Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AnimationFrame time ->
            ( { model | time = Time.posixToMillis time }, Cmd.none )



---- VIEW ----


novemberFirst =
    1572566400000


novemberLast =
    1575111600000


goal =
    222000


monthCompletedView : Element msg
monthCompletedView =
    let
        textFirst =
            Element.paragraph [] [ Element.text "At this point you have written" ]

        wordCount =
            Element.paragraph
                [ Font.size 70 ]
                [ goal
                    |> String.fromInt
                    |> Element.text
                , Element.text " words"
                ]

        textLast =
            Element.paragraph [] [ Element.text "That is a lot of text to write in a single month!" ]
    in
    Element.column
        [ Element.centerX
        , Element.centerY
        , Element.spacing 100
        ]
        [ Element.column
            [ Element.centerX, Element.spacing 10 ]
            [ textFirst
            , wordCount
            , textLast
            ]
        , Element.el [ Element.centerX ] (Element.text "Good work Jo!")
        ]


monthInProgressView : Float -> Element msg
monthInProgressView monthT =
    let
        textFirst =
            Element.paragraph [] [ Element.text "In order to keep pace for your 222k goal, you should have written" ]

        wordCount =
            Element.paragraph
                [ Font.size 70 ]
                [ monthT
                    * goal
                    |> clamp 0 goal
                    |> Round.round 3
                    |> Element.text
                , Element.text " words"
                ]

        textLast =
            Element.paragraph [] [ Element.text "at this point in time." ]
    in
    Element.column
        [ Element.centerX
        , Element.centerY
        , Element.spacing 100
        ]
        [ Element.column
            [ Element.centerX, Element.spacing 10 ]
            [ textFirst
            , wordCount
            , textLast
            ]
        , Element.el [ Element.centerX ] (Element.text "Good luck Jo!")
        ]


view : Model -> Html Msg
view model =
    let
        monthT =
            (toFloat model.time - novemberFirst) / (novemberLast - novemberFirst)
    in
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        , Font.size 30
        ]
        (if monthT < 1 then
            monthInProgressView monthT

         else
            monthCompletedView
        )



---- PROGRAM ----


main : Program Int Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Browser.Events.onAnimationFrame AnimationFrame
        }
